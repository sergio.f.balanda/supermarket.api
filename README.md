- Visual Studio Code
- NET CLI tools

# Create project

- Open the terminal 

        1 - mkdir src/Supermarket.API
        2 - cd src/Supermarket.API
        3 - dotnet new webapi

1-create folder, 2-access to folder, 3-create template

https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-new?tabs=netcore21

# Creating the Domain Models

- Create folders Domain/Models
- Create class Category.cs, Product.cs and EUnitOfMeasure

# Categories API

- Create CategoriesController.cs
- With rute /api/categories
- Add using 
        
        using Microsoft.AspNetCore.Mvc;

- Use suffix

        [Route("/api/[controller]")]

- Add class extends controller  : Controller
- Create in forder domain new directory /Services
- In services folder create interface ICategoryService (An interface tells how something should work, but does not implement the real logic for the behavior). you need to get a list of categories.
- Change the CategoriesController create constructor passing itenface.
- Create method get GetAllAsync, this method response a list async Category. Type [HttpGet].

# Categories Service

- In root folder crete new directory /Services
- crete new class CategoryService this class extends ICategoryService

# Categories Repository and the Persistence Layer

- In domain create Repository folder
- Crete interface ICategoryRepository
- in root folder services complete method, create constructor passing ICategoryRepository

# Database code first

- In root create folder Persistence to access the database
- Create folder Contexts and class AppDbContext, to map models to database tables
- This AppDbContext extends from : DbContext, create constructor. The constructor we added to this class is responsible for passing the database configuration to the base class through dependency injection.
- Create two DbSet  DbSet<Category> Categories, DbSet<Product> Products
- OnModelCreating for create tables with specific field:

        builder.Entity<Category>()
       .HasMany(p => p.Products)
       .WithOne(p => p.Category)
       .HasForeignKey(p => p.CategoryId);

category has many products, set the foreign key CategoryId, 
[more](https://www.learnentityframeworkcore.com/relationships).
- Add a new folder called Repositories inside the Persistence folder, and then add a new class called BaseRepository
- this BaseRepository abstract class receives an instance of our AppDbContext
- On the same folder create CategoryRepository class 
- this class extends base repository, ICategoryRepository, 
the constructor receives AppDbContext and implement ListAsync.

# Dependency Injection
- The Startup class configuring the database context and bind our service and repository
- Change Program class
- In this case I do not follow the documentation as is the documentation because it did not work for me and I do not see some steps
- Startup Configure

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

- Add packages Microsoft.EntityFrameworkCore, Microsoft.EntityFrameworkCore.InMemory, Microsoft.EntityFrameworkCore.Relational
- 

        dotnet run

# Creating a Category Resource
- en root create folder Resources
- create class CategoryResource for return the no real model representation
- add automapper

        dotnet add package AutoMapper

        dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection

- Startup add AutoMapper in ConfigureServices

        services.AddAutoMapper(typeof(Startup));

- in root create folder Mapping and class ModelToResourceProfile
- add mapper to controller, change the constructor to receive IMapper, change GetAllAsync to map IMapper.

# Creating new Categories
- In the Resources folder, add a new class called SaveCategoryResource
- this class has name field
- create post method in controller https post with FromBody SaveCategoryResource
-
# Validating the Request Body Using the Model State
- In controller method post for validations use ModelState.IsValid
- for the error message ModelState.GetErrorMessages()
- create new folder in root Extensions with the class static  ModelStateExtensions, add method GetErrorMessages

# Mapping the new Resource
- in mapping folder add new class ResourceToModelProfile extends to profile
- this class map SaveCategoryResource to category
- in controller map mapper.Map

# Applying the Request-Response Pattern to Handle the Saving Logic
- In the Domain folder, inside Services, add a new directory called Communication. Add a new abstract class there called BaseResponse
- In the same folder, add a new class called SaveCategoryResponse extends BaseResponse and passing category
- in Domain/Services ICategoryService add save method, pass a category 
- in controller call SaveAsync from category service

# The Database Logic and the Unit of Work Pattern
- In ICategoryRepository add AddAsync for the category
- implement this method in CategoryRepository
- most databases implement to save data only after a complex operation finishes to handle this use UnitOfWork
- Add a new interface inside the Repositories folder of the Domain called IUnitOfWork:

        Task CompleteAsync();


- Add a new class called UnitOfWork at the RepositoriesRepositories folder of the Persistence extends IUnitOfWork add constructor 
- complete the SaveAsync in CategoryService
- view controller post

# Testing POST

# Updating Categories
- create method HttpPut wit request id and frombody SaveCategoryResource
 
        [HttpPut("{id}")]

- create in ICategoryService method update
- implement method in CategoryService
- we need add in ICategoryRepository find by id and update 
- implement the logic into the CacontrollertegoryRepository
- CategoryService add logic UpdateAsync

# Deleting Categories
- create method httpDelete in controller with request int id
- create in services/ICategoryService method DeleteAsync
- in ICategoryRepository create new method remove
- And implementation on the repository
- implement the logic on CategoryService

# The Products API
- Add a new controller into the Controllers folder called ProductsController.
- Add a new class ProductResource into the Resources folder with current fields
- configure the mapping between the model class and our new resource class.
create EnumExtensions in Extensions folder (transformation EUnitOfMeasurement enum to a string)
- In Mapping ModelToResourceProfile change add product
- Create IProductService interface with one getList in domain Services
- Create IProductRepository interface with one getList in domain repositories
- Create ProductRepository extends BaseRepository, IProductRepository implement methods
- Implement ProductService extends IProductService
- Let’s bind the new dependencies in startup services
- in controller create constructor passing IProductService and mapper
- create httpget method calling services get list map response 
- create some data in AppDbContext

# Dockerize an ASP.NET
- Download and install Docker (ver si sale un error al ejecutar docker de instalar wsl2)
- crear archivo dockerfile ver nombre del dll
- .dockerignore
- Si sale algun error ver las carpetas COPY ../engine/examples ./
        
        docker build . 

- 
        docker run -d -p 8080:80 (container id)

- 

        docker container ls

- error compatible framework version pase todo 5.0

# Heroku CLI
- instalar heroku cli
- create new app (supermarket-api-net-core)
- select container register view steps
- heroku container:login
- docker build -t YourAppName .
- heroku container:push -a YourAppName web
- heroku container:release -a YourAppName web

        cambiar a
        # ENTRYPOINT ["dotnet", "nombreApp.dll"]
        CMD ASPNETCORE_URLS=http://*:$PORT dotnet nombreApp.dll

        
https://github.com/adnan-kamili/asp.net-core-gitlab-heroku/blob/master/.gitlab-ci.yml
--------------------------------------------------
[doc](https://www.freecodecamp.org/news/an-awesome-guide-on-how-to-build-restful-apis-with-asp-net-core-87b818123e28/)

[Dockerize an ASP.NET Core application](https://docs.docker.com/samples/dotnetcore/)

[buildpack-heroku](https://github.com/jincod/dotnetcore-buildpack)