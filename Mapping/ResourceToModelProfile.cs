﻿using AutoMapper;
using Supermarket.API.Domain.Models;
using Supermarket.API.Services;

namespace Supermarket.API.Mapping
{
    public class ResourceToModelProfile : Profile
    {
        public ResourceToModelProfile()
        {
            CreateMap<SaveCategoryResource, Category>();
        }
    }
}
